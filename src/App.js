import React,{useState,useEffect} from 'react';
import logo from './logo.svg';
import './App.css';

import Header from './header/Header'
import Slider from './Slider/Slider'
import {BrowserRouter as Router,Switch,Route,IndexRoute} from 'react-router-dom'
import Features from './Features/Features';
import Nav from './Nav/Nav'
import {ProductContextProvider} from './ProductContext'
import ProductList from './Product/ProductList';
import axios from 'axios'

import '../node_modules/alertifyjs/build/css/alertify.min.css'
import '../node_modules/alertifyjs/build/css/themes/default.min.css'
import '../node_modules/alertifyjs/build/alertify.js'

import {alertifyjs} from 'alertifyjs'

function App() {
  const [products,setProducts]=useState([])

    useEffect(()=>{
        axios.get("http://localhost/react/products.php?products")
        .then(
            res=>{
                setProducts(...products,res.data.lists)
            })
        .catch(err=>{
            console.log(err)
        })
    },[])


  return (
    
    <Router>
      <div className="App">
          <Nav/>
            <Switch>
                
                    <Route path="/" exact component={Header}/>
                    <Route path="/features" component={Features}/>
                    <ProductContextProvider value={products}>
                      <Route path="/products" component={ProductList}/>
                    </ProductContextProvider>
               
            </Switch>
      </div>
    </Router>
  );
}

export default App;
