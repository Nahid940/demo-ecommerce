
let  cart=[];
export function addToCart(data)
{
    if(localStorage.cart)
    {
        cart=JSON.parse(localStorage.cart)
    }

	var id = data.productID;
    var qty = 1;
    var image=data.product_image
    var price=100
    var subtotal=price

    for (var i in cart) {
		if(cart[i].id == id)
		{
            cart[i].qty+=1;
            cart[i].subtotal=price*cart[i].qty
            saveCart()
			return;
        }
    }

    var item = { id: id, qty: qty ,image:image,price:price,subtotal:subtotal}; 
    cart.push(item);
    saveCart()
    
}

const saveCart=()=>
{
    if (window.localStorage)
	{
        localStorage.cart = JSON.stringify(cart);
	}
}


export function getCartData()
{
    return JSON.parse(localStorage.cart);
}