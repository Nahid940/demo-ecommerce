import React from 'react'

const Slider=()=>
{
    return (
        <>
            <div className="slider">
                <div id="about-slider">
                    <div id="carousel-slider" className="carousel slide" data-ride="carousel">
                        <ol className="carousel-indicators visible-xs">
                        <li data-target="#carousel-slider" data-slide-to="0" className="active"></li>
                        <li data-target="#carousel-slider" data-slide-to="1"></li>
                        <li data-target="#carousel-slider" data-slide-to="2"></li>
                        </ol>
                    </div>

                    <div className="carousel-inner">
                        <div className="item active">
                            <img src="img/7.jpg" className="img-responsive" alt=""/>
                            <div className="carousel-caption">
                            <div className="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.3s">
                                <h2><span>Clean & Fully Modern Design</span></h2>
                            </div>
                            <div className="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.6s">
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing</p>
                            </div>
                            <div className="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.9s">
                                <form className="form-inline">
                                <div className="form-group">
                                    <button type="livedemo" name="Live Demo" className="btn btn-primary btn-lg" required="required">Live Demo</button>
                                </div>
                                <div className="form-group">
                                    <button type="getnow" name="Get Now" className="btn btn-primary btn-lg" required="required">Get Now</button>
                                </div>
                                </form>
                            </div>
                            </div>
                        </div>


                        <div className="item">
                            <img src="img/6.jpg" className="img-responsive" alt=""/>
                            <div className="carousel-caption">
                            <div className="wow fadeInUp" data-wow-offset="0" data-wow-delay="1.0s">
                                <h2>Fully Responsive</h2>
                            </div>
                            <div className="wow fadeInUp" data-wow-offset="0" data-wow-delay="1.3s">
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing</p>
                            </div>
                            <div className="wow fadeInUp" data-wow-offset="0" data-wow-delay="1.6s">
                                <form className="form-inline">
                                <div className="form-group">
                                    <button type="livedemo" name="purchase" className="btn btn-primary btn-lg" required="required">Live Demo</button>
                                </div>
                                <div className="form-group">
                                    <button type="getnow" name="subscribe" className="btn btn-primary btn-lg" required="required">Get Now</button>
                                </div>
                                </form>
                            </div>
                            </div>
                        </div>

                        <div className="item">
                            <img src="img/1.jpg" className="img-responsive" alt=""/>
                            <div className="carousel-caption">
                            <div className="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.3s">
                                <h2>Modern Design</h2>
                            </div>
                            <div className="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.6s">
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing</p>
                            </div>
                            <div className="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.9s">
                                <form className="form-inline">
                                <div className="form-group">
                                    <button type="livedemo" name="purchase" className="btn btn-primary btn-lg" required="required">Live Demo</button>
                                </div>
                                <div className="form-group">
                                    <button type="getnow" name="subscribe" className="btn btn-primary btn-lg" required="required">Get Now</button>
                                </div>
                                </form>
                            </div>
                            </div>
                        </div>
                    </div>

                    <a className="left carousel-control hidden-xs" href="#carousel-slider" data-slide="prev">
                        <i className="fa fa-angle-left"></i>
                    </a>

                    <a className=" right carousel-control hidden-xs" href="#carousel-slider" data-slide="next">
                        <i className="fa fa-angle-right"></i>
                    </a>
                </div>
            </div>
        </>
    )
}

export default Slider