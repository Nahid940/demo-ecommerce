import React from 'react'

const ProductContexts= React.createContext({})

export const ProductContextProvider=ProductContexts.Provider

export default ProductContexts
