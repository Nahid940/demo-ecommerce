import React from 'react'
import {Link}  from 'react-router-dom'
const Nav=()=>
{
    return (
        <>
            <header id="header">
                <nav className="navbar navbar-fixed-top" role="banner">
                    <div className="container">
                        <div className="navbar-header">
                        <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                        <span className="sr-only">Toggle navigation</span>
                                        <span className="icon-bar"></span>
                                        <span className="icon-bar"></span>
                                        <span className="icon-bar"></span>
                                    </button>
                        <a className="navbar-brand" href="index.html">Bikin</a>
                        </div>
                        <div className="collapse navbar-collapse navbar-right">
                            <ul className="nav navbar-nav">
                                <li ><Link to={"/"} >Home</Link></li>
                                <li><Link to={"/features"}>Features</Link></li>
                                <li><Link to={"/products"}>Products</Link></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </header>
        </>
    )
}

export default Nav