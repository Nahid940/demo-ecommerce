import React,{useEffect} from 'react'

const Features=()=>
{

    useEffect(()=>{
        console.log("ok")
    })

    return (
        <>
            <div id="feature">
                <div className="container">
                    <div className="row">
                        <div className="text-center">
                            <h3>Features</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit Lorem ipsum dolor sit<br/>amet consectetur adipisicing elit</p>
                        </div>
                        <div className="col-md-3 wow fadeInRight" data-wow-offset="0" data-wow-delay="0.3s">
                            <div className="text-center">
                                <div className="hi-icon-wrap hi-icon-effect">
                                <i className="fa fa-laptop"></i>
                                <h2>Fully Responsive</h2>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-3 wow fadeInRight" data-wow-offset="0" data-wow-delay="0.3s">
                            <div className="text-center">
                                <div className="hi-icon-wrap hi-icon-effect">
                                    <i className="fa fa-heart-o"></i>
                                    <h2>Retina Ready</h2>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-3 wow fadeInLeft" data-wow-offset="0" data-wow-delay="0.3s">
                            <div className="text-center">
                                <div className="hi-icon-wrap hi-icon-effect">
                                    <i className="fa fa-cloud"></i>
                                    <h2>Easily Customize</h2>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-3 wow fadeInLeft" data-wow-offset="0" data-wow-delay="0.3s">
                            <div className="text-center">
                                <div className="hi-icon-wrap hi-icon-effect">
                                    <i className="fa fa-camera"></i>
                                    <h2>Quality Code</h2>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Features