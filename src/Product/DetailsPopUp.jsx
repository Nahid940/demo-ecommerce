import React from 'react'


const DetailsPopUP=({product,show,handleClose})=>{
    return (
        <>
            <div className={(show)?"modal show":"modal"} tabIndex="-1" id="exampleModal" role="dialog">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">Product Details</h5>
                        <button type="button" className="close" onClick={handleClose} aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <img src={"https://d1p8kb93vav29r.cloudfront.net/"+product.product_image} alt="" style={{width:'40%'}}/>
                        <h2>{product.product_title}</h2>
                        <p>{product.product_details}</p>
                    </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default DetailsPopUP