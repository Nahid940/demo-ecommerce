import React,{useContext,useEffect,useState} from 'react'
import ProductContexts from '../ProductContext'
import DetailsPopUp from './DetailsPopUp'
import {addToCart,getCartData} from '../Helper'
// import {ProductProvider} from '../App'

const ProductList=()=>
{

    const products=useContext(ProductContexts)

    const [modal,setModal]=useState(false)

    const [singleproduct,setSingleProduct]=useState()

    const showDetails=(data)=>{
        setSingleProduct(data)
        setModal(true)
        addToCart(data)
        // console.log(getCartData())
    }
    const handleClose=()=>{
        setModal(false)
    }

    return (
        <>
        <div id="feature">
                <div className="container">
                    <div className="row">
                        <div className="text-center">
                            <h3>
                                
                            </h3>
                        </div>
                        {products.map(product=>(
                            <div key={product.productID} className="col-md-3 wow fadeInRight" data-wow-offset="0" data-wow-delay="0.3s">
                                <div className="text-center"  className={{height:'300px'}}>
                                    <div className="hi-icon-wrap hi-icon-effect">
                                    <img src={"https://d1p8kb93vav29r.cloudfront.net/"+product.product_image} alt="" style={{width:'100%'}}/>
                                    <h2>{product.product_title}</h2>
                                    <p>52345</p>
                                    <button className="btn btn-success" onClick={()=>showDetails(product)}>Add to cart</button>
                                    </div>
                                </div>
                            </div>
                        ))}
                        {/* {(modal)?<DetailsPopUp product={singleproduct} show={true} handleClose={handleClose}/>:""} */}
                    </div>
                </div>
            </div>
        </>
    )
}

export default ProductList